# docker-hetzner-dynDNS-updater

Updates and set specific DNS A record value to client-IP address on Hetzner DNS through API. Edit env file then build and run.

# to build
docker build -t hetzner-dns-updater .

# to run
docker run --rm -it --name hetzner-dns-updater --env-file dns-updater.env -p 5000:5000 hetzner-dns-updater:latest
