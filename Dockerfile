FROM alpine:latest

MAINTAINER gurcan@gurcanozturk.com

COPY ./hetzner_dns_updater.py /
COPY ./run.sh /

RUN apk update && apk add py3-requests py3-pip && \
    pip install flask && \
    chmod +x /run.sh

ENTRYPOINT ["/run.sh"]
