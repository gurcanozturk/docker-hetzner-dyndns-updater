from flask import Flask
from flask import request

import logging
import requests
import json
import sys
import os

def get_zone_id(domain):
        r = requests.get(
            url=("https://dns.hetzner.com/api/v1/zones?name=%s" % (domain)),
            headers={
                "Auth-API-Token": token,
            },
        )

        if r.status_code == 200:
          content = json.loads(r.content.decode())
          zone_id = content["zones"][0]["id"]
          return zone_id

def get_record_id(zone_id, record_name):
        r = requests.get(url="https://dns.hetzner.com/api/v1/records",
            params={
                "zone_id": zone_id,
            },
            headers={
                "Auth-API-Token": token,
            },
        )

        if r.status_code == 200:
          records = json.loads(r.content.decode())["records"]

          for record in records:
              record_name  = record["name"]
              if record_name == record_name:
                record_id  = record["id"]

          return record_id

def update_record(record_id, record_value, record_ttl, record_type, record_name, zone_id):
        r = requests.put(url=("https://dns.hetzner.com/api/v1/records/%s" % (record_id)),
            headers={
                "Content-Type": "application/json",
                "Auth-API-Token": token,
            },
            data=json.dumps({
              "value": record_value,
              "ttl": int(record_ttl),
              "type": record_type,
              "name": record_name,
              "zone_id": zone_id
            })
        )
        if r.status_code == 200:
           return (r.content)
        else:
           logging.warning("Warning! Record not updated: %s" % (r.content))

token  = os.environ['DNS_TOKEN']
record = os.environ['DNS_RECORD']

record_name, domain = record.split('.', 1)
zone_id     = get_zone_id(domain)
record_id   = get_record_id(zone_id, record)

app = Flask(__name__)

@app.route('/')
def index():
    ip = request.environ.get('HTTP_X_FORWARDED_FOR', request.remote_addr)
    update = update_record(record_id, ip, 60, "A", record_name, zone_id)
    logging.warning("Record updated: %s" % (update))
    logging.warning("Client IP: %s" % (ip))
    return 'OK. Record updated'

if __name__ == '__main__':
   app.run()
